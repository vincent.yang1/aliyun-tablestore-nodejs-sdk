const client = require('./client');
const TableStore = require('../index.js');
const Long = TableStore.Long;
const fs = require('fs')
const dayjs = require('dayjs')


async function searchFunc(index){
    const DateFrom = dayjs().startOf('day').add(index, 'day').valueOf()

    const DateTo = dayjs().startOf('day').add(index + 1, 'day').valueOf()
    const fileName =  dayjs().startOf('day').add(index, 'day').format('YYYY_MM_DD')

    console.log(DateFrom)
    console.log(DateTo)
    const result = await client.search({
        tableName: 'cfd_activity_meta',
        indexName: 'index_search_activity_meta_all',
        searchQuery: {
            offset: 0,
            limit: 1000, //如果只为了获取行数，无需获取具体数据，可以设置limit=0，即不返回任意一行数据。

            query: {
                queryType: TableStore.QueryType.BOOL_QUERY,
                query: {
                    shouldQueries: [
                        {
                            //设置查询类型为TableStore.QueryType.RANGE_QUERY。
                            queryType: TableStore.QueryType.RANGE_QUERY,
                            query: {
                                fieldName: "activity_time",
                                rangeFrom: DateFrom,
                                includeLower: true, //包括下边界（即大于等于1）。
                                rangeTo: DateTo,
                                includeUpper: DateTo //不包括上边界（即小于10）。
                            }

                        },
                        { //设置查询类型为TableStore.QueryType.TERM_QUERY。
                            queryType: TableStore.QueryType.TERM_QUERY,
                            query: {
                                fieldName: "verb",
                                term: "1"
                            }
                        }
                    ],
                    minimumShouldMatch:2
                }

            },
            sort: {
                sorters: [
                    {
                        fieldSort: {
                            fieldName: "activity_time",
                            order: TableStore.SortOrder.SORT_ORDER_DESC
                        }
                    }
                ]
            },
            getTotalCount: false //结果中的TotalCount可以表示表中数据的总行数，默认为false，表示不返回。
        },
        columnToGet: { //返回列设置，可设置为RETURN_SPECIFIED（自定义返回列）、RETURN_ALL（返回所有列）、RETURN_ALL_FROM_INDEX（返回多元索引中的所有列）、RETURN_NONE（不返回）。
            returnType: TableStore.ColumnReturnType.RETURN_SPECIFIED,
            returnNames: ['customer_id','activity_id','activity_time']
        },

    });
    console.log('success:',  fileName, result.rows.length);
    fs.writeFileSync(fileName + '.txt', JSON.stringify(result))
//     if(result.)
// , function (err, data) {
//         if (err) {
//             console.log('error:', err);
//             return;
//         }
        
//         // fs.writeFileSync(fileName + '.txt', JSON.stringify(data))
//         console.log('success:', JSON.stringify(data, null, 2), data.rows.length);
//     }


}

(async function main(){
   const arr = Array.from(Array(2).keys())
   
    await Promise.all(arr.map(async (val, index) => {
        console.log(-val)
        await searchFunc(-val)
    }));
})()