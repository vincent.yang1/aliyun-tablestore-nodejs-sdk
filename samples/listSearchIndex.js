var client = require('./client');

client.listSearchIndex({
    tableName: "cfd_activity_meta"
}, function (err, data) {
    if (err) {
        console.log('error:', err);
        return;
    }
    console.log('success:', JSON.stringify(data, null, 2));
});

